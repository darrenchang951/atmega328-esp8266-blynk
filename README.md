
# Things you need
## Parts
- ESP8266
- ATmega328
- Breadboard or anything that you can use to connect the wires
- Whatever you want to control (example: a relay since I created this so I can turn on and off my fan)

## Tools
- Arduino Uno (This is used to set baud rate).
- Arduino software (This is used to compile the scratch and set baud rate) *Version 1.6.13.*. 
[Download here](https://www.arduino.cc/en/Main/OldSoftwareReleases).
- Hardware configuration [breadboard-1-6-x.zip](https://www.arduino.cc/en/uploads/Tutorial/breadboard-1-6-x.zip). This 
can be found at: <https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard>.
- Raspberry Pi (This is used as a programmer because it has GPIO)

# Getting started
## Set the baud rate for your ESP8266
ESP8266 defaults to 115200 baud rate, but we intend to use ATmega at 8MHz, 
which can only accurately understand ESP8266 at 38400 baud rate. I know
this because I checked
[AVR Baud Rate Tables](https://cache.amobbs.com/bbs_upload782111/files_22/ourdev_508497.html).

1. Connect your ATmega328 to your Arduino Uno as shown in the diagram below.
```
AT+UART_DEF=38400,8,1,0,0
```

## Compile the sketch using Arduino software (Because I don't know how to compile on Raspberry Pi)
Upload the software to your ATMega328. This software will be using the 
internal Oscilator, which runs at 8MHz. This is why the baud rate had to 
be changed 38400 so ATmega328 and ESP8266 can understand each other.

Compile `esp8266-blynk.ino` using Arduino software.
1. Go to Tools->Board and select `ATmega328 on a breadboard (8MHz internal clock)`
2. Sketch->Export compiled Binary. This will generate two .hex files. One with 
bootloader and one without bootloader. The one without bootloader is used since 
I have no clue how to perform updates with bootloader anyways.
3. Somehow transfer that .hex file to your RaspberryPi.

## Upload the .hex to your ATmega328
1. Connect your ATmega328 to your RaspberryPi as shown in the diagram below.
![](raspberrypi_atmega328.png)
2. Check the connection. ```avrdude -p m328p -c gpio```
3. Upload the .hex file to your ATmega328. 
```avrdude -p m328p -c gpio -e -U flash:w:<name_of_the_file.hex>```

## Connect ATmega328 and ESP8266 and done
![](circuit_bb.png)

# Things I need help with
1. This project is kinda crappy. Let me know what I should improve.
2. Something is wrong with my `Makefile`. I cannot compile on my RaspberryPi.
3. How to use a programmer to upload? It seems very convenient as I won't need to use a 
raspberry pi as a programmer anymore.
4. Many other things that I don't even know about.
